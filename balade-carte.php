<?php

/* Template Name: Balade-carte */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>

<?php
/**
    *   TEMPLATE POUR LA CARTE
    *
    */

global $post;
$slug = $post->post_name;

$balade = get_the_title();
$category = get_category_by_slug($slug)->cat_ID;

// $projets = new WP_Query(array( "category" => $category));
$projets = get_posts(array("category" => $category, "post_type" => "post", "numberposts" => -1));

$intro_text = get_field('intro_text', 'category_' . $category);
$cover_text = get_field('cover_text', 'category_' . $category);
$more_info_left = get_field('more_info_left', 'category_' . $category);
$more_info_right = get_field('more_info_right', 'category_' . $category);
$logo = get_field('logo', 'category_' . $category);
$map_spot_1 = get_field('map_spot_1', 'category_' . $category);
$map_spot_2 = get_field('map_spot_2', 'category_' . $category);
$institutions_support = get_field('institutions_support', 'category_' . $category);

?>

  </head>
<body <?php body_class(); ?>>

    <article id="recto" style="--local-bg: var(--spot-fonce)">

    <div class="fondperdu"></div>
      <section class="couv-1e spot1">
      <div class="cercle">
          <h1><?= $balade ?></h1>
        </div>
        <div class="couv-images">
            <?php foreach($projets as $page) : ?>

            <?php
    $img_url = wp_get_attachment_url(get_post_thumbnail_id($page->ID), 'thumbnail');
                ?>

                <div class="image" >
                  <img src="<?= $img_url ?>" />
                <aside class="img-color"></aside>
              </div>
            <?php endforeach; ?>

        </div>

	<svg class="svg-couv tour-guide" viewBox="0 0 297.6 419.5">
          <?php
          $fichier = '';

switch ($cover_text["value"]) {
    case 'bma':
        $fichier = 'bma';
        break;

    case 'fga-vb':
        $fichier = 'vb';
        break;

    case 'fgb-wbf':
        $fichier = 'fga-fwb';
        break;

    case 'c-bma':
        $fichier = 'cb';
        break;

    default:
        $fichier = 'tour-guide';
        break;
}

$filepath = get_site_url() . '/wp-content/themes/bma-tour-guide/assets/covers/' . $fichier . '.svg#tour-guide';
?>

        <use xlink:href="<?= $filepath ?>" fill="white"/>
	</svg>

      </section>

      <div class="more_info_left spot1">
        <?php if ($more_info_left) : ?>
          <?= $more_info_left ?>
        <?php endif ?>
      </div>


      <div class="more_info_right spot1">
        <?php if ($more_info_right) : ?>
          <?= $more_info_right ?>
        <?php endif ?>
      </div>

      <section class="couv-4e">

        <div class="intro-text">
          <?= $intro_text ?>
        </div>

      <div class="credits">

        <p class="qrcode">Find a web version of this map and other tours through this link: <br/><a href="https://rb.gy/mpyb56">rb.gy/mpyb56</a></p>
          <!-- QR CODE À REMPLACER -->
        <!-- <img src="https://cdn.kibrispdr.org/data/910/white-qr-code-png-35.png" alt="qr code" /> -->
          <svg viewBox="0 0 1155 1155">
            <use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/qr-code.svg#qrcode" />
          </svg>

          <?php if ($logo) : ?>
            <div class="logo" style="background-image: url('<?= $logo ?>');"></div>
          <?php endif ?>


          <?php if ($institutions_support) : ?>
             <?= $institutions_support ?>
          <?php endif ?>

        <p class="real-credits">Design & Dev: Kidnap your designer with Thomas Bris • Print: Drifosett • © MapTiler © OpenStreetMap Contributors © WaterGIS
        </p>

      </div>

        <svg class="svg-couv back-cover" viewBox="0 0 297.6 419.5">
          <use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/covers/dos.svg#tour-guide" />
	</svg>

      </section>

   </article>
    <article id="verso" style="--local-bg: var(--spot-fluo )">
    <div class="fondperdu"></div>

      <?php if ($map_spot_1) : ?>
        <?php
            $map_infos = explode('_', $map_spot_1["title"]);

          $zoom = $map_infos[1];
          $scale = $map_infos[2];
          $bearing = $map_infos[3];
          ?>

      <div class="map-wrapper">
        <img class="spot2" src="<?= $map_spot_2["url"] ?>" alt="map spot fluo" />
        <img class="inverse" src="<?= $map_spot_1["url"] ?>" alt="map spot dark" />
      </div>

        <div id="cache-misere">
        </div>

        <svg id="boussole" style="--boussole-rotation: <?= $bearing ?>;" viewBox="0 0 16 16">
          <use xlink:href="<?= get_site_url() . '/wp-content/themes/bma-tour-guide/assets/pointeurs/boussole.svg#boussole'?>" fill="white"/>
                </svg>


      <?php else : ?>
      <h2> Add map exports to see them here</h2>

      <?php endif; ?>

    </article>

    <?php wp_footer(); ?>
</body>
