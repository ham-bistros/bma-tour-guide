function resetColors() {
  document.getElementById("spot-color").value = "normal";

  document.documentElement.style.setProperty("--spot-fonce", "#004c5d");
  document.documentElement.style.setProperty("--spot-fluo", "#ff436c");
  document.body.classList.remove("hide-spot-1");
  document.body.classList.remove("hide-spot-2");
}

function spot_1Colors() {
  document.getElementById("spot-color").value = "spot-1";

  document.documentElement.style.setProperty("--spot-fonce", "black");
  document.documentElement.style.setProperty("--spot-fluo", "white");
  document.body.classList.add("hide-spot-2");
}

function spot_2Colors() {
  document.getElementById("spot-color").value = "spot-2";

  document.documentElement.style.setProperty("--spot-fluo", "black");
  document.documentElement.style.setProperty("--spot-fonce", "white");
  document.body.classList.add("hide-spot-1");
}

function nicePrint() {
  const printHeader = document.querySelector("header.print-controls");
  printHeader.style.display = "none";

  window.addEventListener("afterprint", () => {
    printHeader.style.display = "block";
    console.log("page printed !");
  });

  const spotColor = document.getElementById("spot-color").value;
  const url = window.location.href.split("/");
  const filetype = url[url.length - 3] == "category" ? "depliant" : "carte";

  document.title = `${filetype}_${url[url.length - 2]}_${spotColor}`;
  window.print();
}

class headerButtons extends Paged.Handler {
  // this let us call the methods from the the chunker, the polisher and the caller for the rest of the script
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered() {
    const header = document.createElement("header");
    header.classList.add("print-controls");
    header.innerHTML = `
      <button class="reset" onclick="resetColors()">RESET</button>
      <button class="spot_1" onclick="spot_1Colors()">SELECT SPOT_1</button>
      <button class="spot_2" onclick="spot_2Colors()">SELECT SPOT_2</button>
      <button class="print reset" onclick="nicePrint()">PRINT</button>
      <input type="hidden" id="spot-color" value="normal"></input>
`;

    document.body.prepend(header);
  }
}

Paged.registerHandlers(headerButtons);
