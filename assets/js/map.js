//////////////////// FUNCTIONS
async function getPosts(postType) {
  const response = await fetch(
    `https://tourguide.bma.brussels/wp-json/wp/v2/${postType}?per_page=100`,
  ); // If there were more than 100 posts per post type, it would be necessary to implement a loop through all pages
  const posts = await response.json();

  return posts;
}

async function getCategories() {
  const response = await fetch(
    `https://tourguide.bma.brussels/wp-json/wp/v2/categories`,
  );
  const categories = await response.json();
  return categories;
}

async function getMedia(id) {
  const response = await fetch(
    `https://tourguide.bma.brussels/wp-json/wp/v2/media/${id}`,
  );
  const media = await response.json();
  return media.source_url;
}

function getPostURL(post) {
  const catURL = `https://tourguide.bma.brussels/category/${categoriesSlugs[post.categories[0]]}`;

  const postURL =
    post.type == "non-detaille"
      ? `${catURL}#non-detaille`
      : `${catURL}#${post.slug}`;
  return postURL;
}

// Convert posts to geoJSON
function posts2geojson(postType, geojsonObject) {
  getPosts(postType)
    .then((posts) => {
      posts.forEach((post) => {
        if (post.acf.latitude && post.acf.longitude) {
          geojsonObject.features.push({
            type: "Feature",
            geometry: {
              type: "Point",
              coordinates: [post.acf.longitude, post.acf.latitude],
            },
            properties: {
              title: post.title.rendered,
              index: `${post.acf.index}\n`,
              marker: post.acf.viewpoint
                ? "pointeur-oeil-vert"
                : "pointest-ND-vert",
              markerNoir: post.acf.viewpoint
                ? "pointeur-oeil-noir"
                : "pointest-ND-noir",
              textOffset: post.acf.viewpoint ? [0, -1.4] : [0, -2],
              iconSize: post.acf.viewpoint ? 3.2 : 3.8,
              info_fields: getInfoFields(post),
              postURL: getPostURL(post),
            },
          });

          // Adds coordinates to the array to calculate bounds of the tour
          const catSlug = categoriesSlugs[post.categories[0]];
          try {
            noPathCoordinates[catSlug].push([
              post.acf.longitude,
              post.acf.latitude,
            ]);
          } catch (e) {
            console.log(catSlug, e);
            window.location.reload(true); // Adds an auto reload if things are not loaded properly (gross)
          }
        }
      });
    })
    .catch((err) => {
      console.log(err);
    });
}

function getInfoFields(post) {
  const fields = [
    "conception",
    "prime_contractor",
    "date",
    "program",
    "adress",
    "website",
  ];

  let obj = {};
  for (const field of fields) {
    if (post.acf[field]) {
      obj[field] = post.acf[field];
    }
  }

  return obj;
}

function enablePopups(layer) {
  // When a click event occurs on a feature in the places layer, open a popup at the
  // location of the feature, with description HTML from its properties.
  map.on("click", layer, async (e) => {
    const coordinates = e.features[0].geometry.coordinates.slice();
    const feature = e.features[0].properties;
    const infosFields = JSON.parse(feature.info_fields);
    let string = "";

    for (const info of Object.keys(infosFields)) {
      if (info == "adress" && string != "") {
        string += "<br/>";
      }

      if (info == "website") {
        const url = new URL(infosFields[info]);
        string += `<br/><a href="${infosFields[info]}" target="_blank" class="${info}">${url.hostname}</a>`;
      } else {
        string += `<span class="${info}">${infosFields[info]}</span>`;
      }
    }

    let htmlContent = `
<h4>
  <span class="index ${layer}">${feature.index}</span>
  <span class="nope">${feature.title}<span>
</h4>
<p>${string}</p>
`;

    if (layer == "projets-markers-unclustered") {
      htmlContent += `<a target="_blank" href="${feature.postURL}">More infos</a>`;
    }

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
      coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    new maplibregl.Popup({ className: "maplibre-popup" })
      .setLngLat(coordinates)
      .setHTML(htmlContent)
      .addTo(map);
  });

  // Change the cursor to a pointer when the mouse is over the layer.
  map.on("mouseenter", layer, () => {
    map.getCanvas().style.cursor = "pointer";
  });

  // Change it back to a pointer when it leaves.
  map.on("mouseleave", layer, () => {
    map.getCanvas().style.cursor = "";
  });
}
// Convert gpx path to geoJSON
function path2geojson(path_file, geojsonObject) {
  getMedia(path_file)
    .then((gpxFile) => {
      // Convert gpx to geoJSON for pathing
      fetch(gpxFile)
        .then((response) => response.text())
        .then((text) => {
          const parser = new DOMParser();
          const doc = parser.parseFromString(text, "application/xml");
          const els = doc.getElementsByTagName("trkpt");

          const path = {
            type: "Feature",
            geometry: {
              type: "LineString",
              properties: {},
              coordinates: [],
            },
          };

          for (const pt of els) {
            path.geometry.coordinates.push([
              pt.getAttribute("lon"),
              pt.getAttribute("lat"),
            ]);
          }

          geojsonObject.features.push(path);
        });
    })
    .catch((err) => {
      console.log(err);
    });
}

//////////////////// DEFINITIONS
let localURL;
if (window.location.hostname == "localhost") {
  localURL = `${window.location.origin}/wptest`;
} else {
  localURL = window.location.origin;
}

const themeURL = `${localURL}/wp-content/themes/bma-tour-guide/assets`;

const map = (window.map = new maplibregl.Map({
  container: "map",
  zoom: 12,
  center: [4.33589, 50.84738],
  hash: true,
  style: `${themeURL}/bma-fond-carte_8_OK1-rose.json`,
  // style: `${themeURL}/bma-test.json`,
  maxZoom: 20,
  maxPitch: 85,
}));

// Prepare geojson objects to add them as layers later
const projets = {
  type: "FeatureCollection",
  features: [],
};

const nonDetaille = {
  type: "FeatureCollection",
  features: [],
};

const eatDrink = {
  type: "FeatureCollection",
  features: [],
};

const paths = {
  type: "FeatureCollection",
  features: [],
};

const paths_secondary = {
  type: "FeatureCollection",
  features: [],
};

// Create a lookup table with preloaded categories URLs
let categoriesSlugs = {};

// Create an object to store coordinates of projects for tours that doesn't have a path
let noPathCoordinates = {};

//////////////////// PREPARATION
// Gets the path for each category (== tour)
getCategories()
  .then((categories) => {
    let index = 0;
    categories.forEach((category) => {
      if (category != "blog" && category != "uncategorized") {
        categoriesSlugs[category.id] = category.slug;
        noPathCoordinates[category.slug] = [];

        // Primary path
        if (category.acf.tour) {
          path2geojson(category.acf.tour, paths);
        }
        // Secondary path
        if (category.acf.tour_secondary) {
          path2geojson(category.acf.tour_secondary, paths_secondary);
        }
      }
    });
  })
  .catch((err) => {
    console.log(err);
  });

// getCategories() has to be called first
posts2geojson("posts", projets);
posts2geojson("non-detaille", nonDetaille);

getPosts("eat-drink")
  .then((posts) => {
    const catsCounter = {};

    posts.forEach((post) => {
      if (catsCounter[post.categories[0]]) {
        catsCounter[post.categories[0]] += 1;
      } else {
        catsCounter[post.categories[0]] = 1;
      }

      eatDrink.features.push({
        type: "Feature",
        geometry: {
          type: "Point",
          coordinates: [post.acf.longitude, post.acf.latitude],
        },
        properties: {
          title: post.title.rendered,
          index: `${catsCounter[post.categories[0]]}`,
          info_fields: getInfoFields(post),
          url: post.acf.website,
        },
      });
    });
  })
  .catch((err) => {
    console.log(err);
  });

// Adds compass and zoom controls
map.addControl(
  new maplibregl.NavigationControl({
    visualizePitch: true,
    showZoom: true,
    showCompass: true,
  }),
);

// Adds controls to export the map as an image
map.addControl(
  new MaplibreExportControl.MaplibreExportControl({
    PageSize: MaplibreExportControl.Size.MAP,
    PageOrientation: MaplibreExportControl.PageOrientation.Portrait,
    Format: MaplibreExportControl.Format.PNG,
    DPI: MaplibreExportControl.DPI[500],
    Crosshair: false,
    PrintableArea: true,
    Local: "en",
  }),
  "top-right",
);

map.on("load", async () => {
  // This is the important part of this example: the addLayer
  // method takes 2 arguments: the layer as an object, and a string
  // representing another layer's name. if the other layer
  // exists in the stylesheet already, the new layer will be positioned
  // right before that layer in the stack, making it possible to put
  // 'overlays' anywhere in the layer stack.
  // Insert the layer beneath the first symbol layer.

  // Adding layers in the order of importance : least important layer first, most important layer last

  // Add a line layer for secondary paths
  map.addSource("paths_secondary", {
    type: "geojson",
    data: paths_secondary,
  });

  map.addLayer({
    id: "paths_secondary-lines",
    type: "line",
    source: "paths_secondary",
    layout: {
      "line-join": "round",
      "line-cap": "round",
      visibility: "visible",
    },
    paint: {
      "line-color": "#004c5d",
      "line-dasharray": [1, 2],
      "line-width": 5,
      "line-opacity": 1,
    },
  });

  // Add a line layer for primary paths
  map.addSource("paths", {
    type: "geojson",
    data: paths,
  });

  map.addLayer({
    id: "paths-lines",
    type: "line",
    source: "paths",
    layout: {
      "line-join": "round",
      "line-cap": "round",
      visibility: "visible",
    },
    paint: {
      "line-color": "#004c5d",
      "line-width": 5,
      "line-opacity": 1,
    },
  });

  // Add a symbol layer for eat-drink (displaying text only)
  map.addSource("eat-drink", {
    type: "geojson",
    data: eatDrink,
  });

  map.addLayer({
    id: "eat-drink",
    type: "symbol",
    source: "eat-drink",
    maxzoom: 18,
    minzoom: 13,
    layout: {
      "icon-allow-overlap": true,
      "icon-overlap": "always",
      "text-allow-overlap": true,
      "icon-image": "pointeur-BM-vert",
      "icon-size": 3,
      "text-field": ["get", "index"],
      "text-font": ["quicksand-bold"],
      "text-offset": [0, 0],
      "text-anchor": "center",
      "text-size": 11,
    },
    paint: {
      "text-color": "#004c5d",
    },
  });

  // Add a symbol layer for non-detaille
  map.addSource("non-detaille", {
    type: "geojson",
    data: nonDetaille,
  });

  map.addLayer({
    id: "non-detaille-markers",
    type: "symbol",
    source: "non-detaille",
    maxzoom: 18,
    minzoom: 12,
    layout: {
      "icon-allow-overlap": true,
      "icon-overlap": "always",
      "text-allow-overlap": true,
      "icon-image": ["get", "marker"],
      "icon-size": ["get", "iconSize"],
      "text-size": 11,
      "text-field": ["get", "index"],
      "text-font": ["quicksand-bold"],
      "text-offset": ["get", "textOffset"],
      "text-anchor": "bottom",
      "icon-anchor": "bottom",
    },
    paint: {
      "text-color": "#004c5d",
    },
  });

  // Add a symbol layer for projects
  map.addSource("projets", {
    type: "geojson",
    data: projets,
    cluster: true,
    clusterMaxZoom: 14, // Max zoom to cluster points on
    clusterRadius: 30, // Radius of each cluster when clustering points (defaults to 50)
    clusterProperties: {
      textSum: [
        ["concat", ["accumulated"], ["get", "textSum"]],
        ["get", "index"],
      ],
    },
  });

  map.addLayer({
    id: "projets-markers-unclustered",
    type: "symbol",
    source: "projets",
    filter: ["!", ["has", "point_count"]],
    layout: {
      "icon-allow-overlap": true,
      "text-allow-overlap": true,
      "icon-ignore-placement": true,
      "icon-overlap": "always",
      "icon-image": "pointest-vert",
      "icon-size": 5,
      "text-size": 15,
      "text-field": ["get", "index"],
      "text-font": ["quicksand-bold"],
      "text-offset": [0, -2.4],
      "text-anchor": "bottom",
      "icon-anchor": "bottom",
    },
    paint: {
      "text-color": "white",
    },
  });

  map.addLayer({
    id: "projets-markers-clustered",
    type: "symbol",
    source: "projets",
    filter: ["all", ["has", "point_count"], [">", 8, ["get", "point_count"]]],
    layout: {
      "icon-allow-overlap": true,
      "text-allow-overlap": true,
      "icon-ignore-placement": true,
      "icon-overlap": "always",
      "icon-image": "pointest-vert-stretch",
      "icon-text-fit": "both",
      "icon-text-fit-padding": [10, 8, 10, 8],
      "text-field": ["get", "textSum"],
      "text-font": ["quicksand-bold"],
      "text-anchor": "bottom",
      "icon-anchor": "bottom",
    },
    paint: {
      "text-color": "white",
    },
  });

  map.addLayer({
    id: "projets-markers-clustered-far",
    type: "symbol",
    source: "projets",
    filter: ["all", ["has", "point_count"], ["<", 8, ["get", "point_count"]]],
    layout: {
      "icon-allow-overlap": true,
      "text-allow-overlap": true,
      "icon-ignore-placement": true,
      "icon-overlap": "always",
      "icon-image": "pointest-vert-stretch",
      "icon-text-fit": "both",
      "icon-text-fit-padding": [10, 11, 20, 11],
      "text-field": ["concat", ["get", "point_count"], ["literal", "p"]],
      "text-font": ["quicksand-bold"],
      "text-anchor": "bottom",
      "icon-anchor": "bottom",
    },
    paint: {
      "text-color": "white",
    },
  });
});

// //////////////////// POPUPS

// Change the cursor to a X when it is on a clustered layer
map.on("mouseenter", "projets-markers-clustered", () => {
  map.getCanvas().style.cursor = "not-allowed";
});

// Change it back to a pointer when it leaves.
map.on("mouseleave", "projets-markers-clustered", () => {
  map.getCanvas().style.cursor = "";
});

// Change the cursor to a X when it is on a clustered-far layer
map.on("mouseenter", "projets-markers-clustered-far", () => {
  map.getCanvas().style.cursor = "not-allowed";
});

// Change it back to a pointer when it leaves.
map.on("mouseleave", "projets-markers-clustered-far", () => {
  map.getCanvas().style.cursor = "";
});

enablePopups("non-detaille-markers");
enablePopups("eat-drink");
enablePopups("projets-markers-unclustered");

// //////////////////// INTERFACE
const printButton = document.querySelector(
  ".maplibregl-ctrl-icon.maplibregl-export-control",
);

// Le chemin est toujours '../../bma-fond-carte-noir.json'
printButton.addEventListener("click", () => {
  // calculate scale
  // source: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Resolution_and_Scale

  document
    .querySelector("button.generate-button")
    .addEventListener("click", () => {
      // 	repaint added layers in black
      map.setPaintProperty("paths-lines", "line-color", "black");
      map.setPaintProperty("paths_secondary-lines", "line-color", "black");

      map.setLayoutProperty("eat-drink", "icon-image", "pointeur-BM-noir");
      map.setPaintProperty("eat-drink", "text-color", "black");

      map.setLayoutProperty("non-detaille-markers", "icon-image", [
        "get",
        "markerNoir",
      ]);
      map.setPaintProperty("non-detaille-markers", "text-color", "black");

      map.setLayoutProperty(
        "projets-markers-unclustered",
        "icon-image",
        "pointest-noir",
      );
      map.setLayoutProperty(
        "projets-markers-clustered",
        "icon-image",
        "pointest-noir-stretch",
      );
      map.setLayoutProperty(
        "projets-markers-clustered-far",
        "icon-image",
        "pointest-noir-stretch",
      );

      // 	hide layers
      // (might clean that later)
      map.setLayoutProperty("landcover_grass_fill", "visibility", "none");
      map.setLayoutProperty("landcover_wood_fill", "visibility", "none");
      map.setLayoutProperty("landcover_cemetery_fill", "visibility", "none");
      map.setLayoutProperty("landcover_cemetery_pattern", "visibility", "none");
      map.setLayoutProperty("water", "visibility", "none");
      map.setLayoutProperty("waterway", "visibility", "none");
      map.setLayoutProperty("rail", "visibility", "none");
      map.setLayoutProperty("rail_hatch", "visibility", "none");
      map.setLayoutProperty("road_area_bridge", "visibility", "none");
      map.setLayoutProperty("road_area_pier", "visibility", "none");
      map.setLayoutProperty("road_pier", "visibility", "none");
      map.setLayoutProperty("road_path", "visibility", "none");
      map.setLayoutProperty("road_secondary", "visibility", "none");
      map.setLayoutProperty("road_primary", "visibility", "none");
      map.setLayoutProperty("road_highway_casing", "visibility", "none");
      map.setLayoutProperty("road_highway", "visibility", "none");
      map.setLayoutProperty("building_fill", "visibility", "none");
      map.setLayoutProperty("building_pattern", "visibility", "none");
      map.setLayoutProperty("boundary_state", "visibility", "none");
      map.setLayoutProperty("boundary_country_z5-", "visibility", "none");
      map.setLayoutProperty("boundary_country_z0-4", "visibility", "none");
      map.setLayoutProperty("water_name_lakeline", "visibility", "none");
      map.setLayoutProperty("water_name_way", "visibility", "none");
      map.setLayoutProperty("water_name_sea", "visibility", "none");
      map.setLayoutProperty("water_name_ocean", "visibility", "none");
      map.setLayoutProperty("road_label_primary", "visibility", "none");
      map.setLayoutProperty("road_label_secondary", "visibility", "none");
      map.setLayoutProperty("place_label_park", "visibility", "none");
      map.setLayoutProperty("place_label_village", "visibility", "none");
      map.setLayoutProperty("place_label_city", "visibility", "none");
      map.setLayoutProperty("place_label_town", "visibility", "none");
      map.setLayoutProperty("place_state-label", "visibility", "none");
      map.setLayoutProperty("place_label_country", "visibility", "none");
      map.setLayoutProperty("place_label_continent", "visibility", "none");
      map.setLayoutProperty("Station", "visibility", "none");
      map.setLayoutProperty("background", "visibility", "none");
      map.setLayoutProperty("water", "visibility", "none");
      map.setLayoutProperty("landcover_grass_fill", "visibility", "none");
      map.setLayoutProperty("landcover_grass_pattern", "visibility", "none");
      map.setLayoutProperty("landcover_wood_fill", "visibility", "none");
      map.setLayoutProperty("landcover_wood_pattern", "visibility", "none");
      map.setLayoutProperty("landcover_cemetery_fill", "visibility", "none");
      map.setLayoutProperty("landcover_cemetery_pattern", "visibility", "none");
      map.setLayoutProperty("waterway", "visibility", "none");
      map.setLayoutProperty("rail", "visibility", "none");
      map.setLayoutProperty("rail_hatch", "visibility", "none");
      map.setLayoutProperty("road_area_bridge", "visibility", "none");
      map.setLayoutProperty("road_area_pier", "visibility", "none");
      map.setLayoutProperty("road_pier", "visibility", "none");
      map.setLayoutProperty("road_path", "visibility", "none");
      map.setLayoutProperty("road_secondary", "visibility", "none");
      map.setLayoutProperty("road_primary", "visibility", "none");
      map.setLayoutProperty("road_highway_casing", "visibility", "none");
      map.setLayoutProperty("road_highway", "visibility", "none");
      map.setLayoutProperty("building_fill", "visibility", "none");
      map.setLayoutProperty("building_pattern", "visibility", "none");
      map.setLayoutProperty("boundary_state", "visibility", "none");
      map.setLayoutProperty("boundary_country_z5-", "visibility", "none");
      map.setLayoutProperty("boundary_country_z0-4", "visibility", "none");
      map.setLayoutProperty("water_name_lakeline", "visibility", "none");
      map.setLayoutProperty("water_name_way", "visibility", "none");
      map.setLayoutProperty("water_name_sea", "visibility", "none");
      map.setLayoutProperty("water_name_ocean", "visibility", "none");
      map.setLayoutProperty("road_label_primary", "visibility", "none");
      map.setLayoutProperty("road_label_secondary", "visibility", "none");
      map.setLayoutProperty("Station", "visibility", "none");
      map.setLayoutProperty("place_label_park", "visibility", "none");
      map.setLayoutProperty("place_label_village", "visibility", "none");
      map.setLayoutProperty("place_label_city", "visibility", "none");
      map.setLayoutProperty("place_label_town", "visibility", "none");
      map.setLayoutProperty("place_state-label", "visibility", "none");
      map.setLayoutProperty("place_label_country", "visibility", "none");
      map.setLayoutProperty("place_label_continent", "visibility", "none");
      map.setLayoutProperty("background", "visibility", "none");
      map.setLayoutProperty("landcover_grass_fill", "visibility", "none");
      map.setLayoutProperty("landcover_grass_pattern", "visibility", "none");
      map.setLayoutProperty("landcover_wood_fill", "visibility", "none");
      map.setLayoutProperty("landcover_wood_pattern", "visibility", "none");
      map.setLayoutProperty("landcover_cemetery_fill", "visibility", "none");
      map.setLayoutProperty("water", "visibility", "none");
      map.setLayoutProperty("waterway", "visibility", "none");
      map.setLayoutProperty("rail", "visibility", "none");
      map.setLayoutProperty("rail_hatch", "visibility", "none");
      map.setLayoutProperty("road_area_bridge", "visibility", "none");
      map.setLayoutProperty("road_area_pier", "visibility", "none");
      map.setLayoutProperty("road_pier", "visibility", "none");
      map.setLayoutProperty("road_path", "visibility", "none");
      map.setLayoutProperty("road_secondary", "visibility", "none");
      map.setLayoutProperty("road_primary", "visibility", "none");
      map.setLayoutProperty("road_highway_casing", "visibility", "none");
      map.setLayoutProperty("road_highway", "visibility", "none");
      map.setLayoutProperty("building_fill", "visibility", "none");
      map.setLayoutProperty("building_pattern", "visibility", "none");
      map.setLayoutProperty("boundary_state", "visibility", "none");
      map.setLayoutProperty("boundary_country_z5-", "visibility", "none");
      map.setLayoutProperty("boundary_country_z0-4", "visibility", "none");
      map.setLayoutProperty("water_name_lakeline", "visibility", "none");
      map.setLayoutProperty("water_name_way", "visibility", "none");
      map.setLayoutProperty("water_name_sea", "visibility", "none");
      map.setLayoutProperty("water_name_ocean", "visibility", "none");
      map.setLayoutProperty("road_label_primary", "visibility", "none");
      map.setLayoutProperty("road_label_secondary", "visibility", "none");
      map.setLayoutProperty("place_label_park", "visibility", "none");
      map.setLayoutProperty("place_label_village", "visibility", "none");
      map.setLayoutProperty("place_label_city", "visibility", "none");
      map.setLayoutProperty("place_label_town", "visibility", "none");
      map.setLayoutProperty("place_state-label", "visibility", "none");
      map.setLayoutProperty("place_label_country", "visibility", "none");
      map.setLayoutProperty("place_label_continent", "visibility", "none");
      map.setLayoutProperty("background", "visibility", "none");
      map.setLayoutProperty("landcover_grass_fill", "visibility", "none");
      map.setLayoutProperty("landcover_grass_pattern", "visibility", "none");
      map.setLayoutProperty("landcover_wood_fill", "visibility", "none");
      map.setLayoutProperty("landcover_wood_pattern", "visibility", "none");
      map.setLayoutProperty("landcover_cemetery_fill", "visibility", "none");
      map.setLayoutProperty("landcover_cemetery_pattern", "visibility", "none");
      map.setLayoutProperty("water", "visibility", "none");
      map.setLayoutProperty("waterway", "visibility", "none");
      map.setLayoutProperty("rail", "visibility", "none");
      map.setLayoutProperty("rail_hatch", "visibility", "none");
      map.setLayoutProperty("road_area_bridge", "visibility", "none");
      map.setLayoutProperty("road_area_pier", "visibility", "none");
      map.setLayoutProperty("road_pier", "visibility", "none");
      map.setLayoutProperty("road_path", "visibility", "none");
      map.setLayoutProperty("road_secondary", "visibility", "none");
      map.setLayoutProperty("road_primary", "visibility", "none");
      map.setLayoutProperty("road_highway_casing", "visibility", "none");
      map.setLayoutProperty("road_highway", "visibility", "none");
      map.setLayoutProperty("building_fill", "visibility", "none");
      map.setLayoutProperty("building_pattern", "visibility", "none");
      map.setLayoutProperty("boundary_state", "visibility", "none");
      map.setLayoutProperty("boundary_country_z5-", "visibility", "none");
      map.setLayoutProperty("boundary_country_z0-4", "visibility", "none");
      map.setLayoutProperty("water_name_lakeline", "visibility", "none");
      map.setLayoutProperty("water_name_way", "visibility", "none");
      map.setLayoutProperty("water_name_sea", "visibility", "none");
      map.setLayoutProperty("water_name_ocean", "visibility", "none");
      map.setLayoutProperty("road_label_primary", "visibility", "none");
      map.setLayoutProperty("road_label_secondary", "visibility", "none");
      map.setLayoutProperty("place_label_park", "visibility", "none");
      map.setLayoutProperty("place_label_village", "visibility", "none");
      map.setLayoutProperty("place_label_city", "visibility", "none");
      map.setLayoutProperty("place_label_town", "visibility", "none");
      map.setLayoutProperty("place_state-label", "visibility", "none");
      map.setLayoutProperty("place_label_country", "visibility", "none");
      map.setLayoutProperty("place_label_continent", "visibility", "none");
      map.setLayoutProperty("Station", "visibility", "none");
    });
});

function displayTour(balade) {
  try {
    document.querySelector(".balade-intro.display").classList.toggle("display");
  } catch (e) {
    console.log("Normalement TypeError", e);
  }
  document.querySelector(`.balade-intro.${balade}`).classList.toggle("display");
}

// NAVIGATION
const zoomTo = document.getElementsByClassName("zoomto");

for (let zoomButton of zoomTo) {
  zoomButton.addEventListener("click", (e) => {
    e.preventDefault();
    // Gets the 2nd classname (balade slug) and finds the index of the feature in the paths object
    // Displays the right presentation of the tour
    displayTour(e.target.className.split(" ")[1]);
    coordinates = noPathCoordinates[e.target.className.split(" ")[1]];

    // Pass the first coordinates in the LineString to `lngLatBounds` &
    // wrap each coordinate pair in `extend` to include them in the bounds
    // result. A variation of this technique could be applied to zooming
    // to the bounds of multiple Points or Polygon geomteries - it just
    // requires wrapping all the coordinates with the extend method.
    const bounds = coordinates.reduce(
      (bounds, coord) => {
        return bounds.extend(coord);
      },
      new maplibregl.LngLatBounds(coordinates[0], coordinates[0]),
    );

    map.fitBounds(bounds, {
      padding: 100,
    });
  });
}
