// Determines if the passed element is overflowing its bounds,
// either vertically or horizontally.
// Will temporarily modify the "overflow" style to detect this
// if necessary.
function checkOverflow(el) {
  if (el) {
    var curOverflow = el.style.overflow;

    if (!curOverflow || curOverflow === "visible") el.style.overflow = "hidden";

    var isOverflowing =
      el.clientWidth < el.scrollWidth || el.clientHeight < el.scrollHeight;

    el.style.overflow = curOverflow;

    return isOverflowing;
  } else {
    return false;
  }
}

function moveSingles(startEl, destEl) {
  if (checkOverflow(startEl)) {
    // let singles = startEl.querySelectorAll('div[class*="-single"]');
    let singles = startEl.querySelectorAll("div.ed-single, div.nd-single");
    let fragment = document.createDocumentFragment();

    if (singles[singles.length - 1]) {
      fragment.appendChild(singles[singles.length - 1]);
      destEl.prepend(fragment);

      moveSingles(startEl, destEl);
    }
  } else {
    console.log("DEPLIANT.JS → Cleaned overflow");
  }
}

class testClass extends Paged.Handler {
  // this let us call the methods from the the chunker, the polisher and the caller for the rest of the script
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  afterRendered(pages) {
    const alongTheWay = document.querySelectorAll("section.non-detaille");

    // if (alongTheWay.length > 1) {
    //   moveSingles(alongTheWay[0], alongTheWay[1]);
    // }
    const param = window.location.hash;
    if (param) {
      console.log(param);
      location.hash = `${param}`;
    }
  }
}
Paged.registerHandlers(testClass);
