<?php
/* Registers theme support for a given feature.
*
* @link https://developer.wordpress.org/reference/functions/add_theme_support/
*/
add_action('init', 'theme_support');

if (! function_exists('theme_support')) {

    function theme_support()
    {

        /**
         * This feature enables Post Thumbnails support for a theme. Note that you can optionally pass a second argument, $args, with an array of the Post Types for which you want to enable this feature.
         *
         * @link https://developer.wordpress.org/reference/functions/add_theme_support/#post-thumbnails
         */
        add_theme_support('post-thumbnails');

    }
}

// Register print query parameter so WP can read it later
function custom_query_vars($qvars)
{
    $qvars[] = 'print';
    return $qvars;
}

// If print parameter == 1, add a custom print stylesheet
function check_page_type()
{

    $page_tpl = get_page_template();
    $page_tpl = explode('/', get_page_template());
    if (end($page_tpl) == 'balade-carte.php') {
        wp_enqueue_style('bma-tour-guide-print', '/wp-content/themes/bma-tour-guide/assets/css/carte-print.css');
        wp_register_style('bma-tour-guide-print-interface', '/wp-content/themes/bma-tour-guide/assets/css/interface.css');
        wp_enqueue_style('bma-tour-guide-print-interface');

        wp_enqueue_script('pagedjs', '/wp-content/themes/bma-tour-guide/assets/js/paged.polyfill.js', array(), '1.0.0', array( 'strategy'  => 'defer',));
    } elseif (get_query_var('cat')) {
        wp_enqueue_style('bma-tour-guide-print', '/wp-content/themes/bma-tour-guide/assets/css/depliant-print.css');
        wp_register_style('bma-tour-guide-print-interface', '/wp-content/themes/bma-tour-guide/assets/css/interface.css');
        wp_enqueue_style('bma-tour-guide-print-interface');


        wp_enqueue_script('pagedjs', '/wp-content/themes/bma-tour-guide/assets/js/paged.polyfill.js', array(), '1.0.0', array( 'strategy'  => 'defer',));

        if (!is_user_logged_in()) {
            wp_enqueue_style('bma-tour-guide-print-homepage', '/wp-content/themes/bma-tour-guide/assets/css/depliant-web.css');
            wp_enqueue_style('bma-tour-guide-print-homepage', '/wp-content/themes/bma-tour-guide/assets/css/depliant-web.css');
        }
    }


}

// Add base theme stylesheet (style.css)
function theme_enqueue()
{
    wp_enqueue_style('bma-tour-guide', get_stylesheet_uri());
}

function load_map_assets()
{
    if (is_front_page()) {
        // styles
        wp_enqueue_style('maplibre-export-style', "https://www.unpkg.com/@watergis/maplibre-gl-export@3.0.1/dist/maplibre-gl-export.css");
        wp_enqueue_style('maplibre-style', "https://unpkg.com/maplibre-gl@latest/dist/maplibre-gl.css");
        wp_enqueue_style('bma-tour-guide-print', '/wp-content/themes/bma-tour-guide/assets/css/map.css');

        // scripts
        wp_enqueue_script('maplibre-script', "https://unpkg.com/maplibre-gl@latest/dist/maplibre-gl.js");
        // wp_enqueue_script('maplibre-export-script', "https://www.unpkg.com/@watergis/maplibre-gl-export@3.0.1/dist/maplibre-gl-export.umd.js");
        wp_enqueue_script('maplibre-export-script', '/wp-content/themes/bma-tour-guide/assets/js/maplibre-gl-export.umd.js'); // custom one
        wp_enqueue_script('map_functions', "/wp-content/themes/bma-tour-guide/assets/js/map.js", array(), '1.0.0', array('strategy'  => 'defer',));
    }
}

// add_filter('query_vars', 'custom_query_vars');

add_action('wp_enqueue_scripts', 'theme_enqueue');
add_action('wp_enqueue_scripts', 'check_page_type');

add_action('wp_enqueue_scripts', function () {
    if (get_query_var('cat')) {
        wp_enqueue_script('depliant', '/wp-content/themes/bma-tour-guide/assets/js/depliant.js', array('pagedjs'), '1.0.0', array());
    }

    if (!is_front_page() && is_user_logged_in()) {
        wp_enqueue_script('export', '/wp-content/themes/bma-tour-guide/assets/js/export.js', array('pagedjs'), '1.0.0', array());
    }
});

// Add map stylesheet if page is frontpage
add_action('wp_enqueue_scripts', 'load_map_assets');

add_filter('big_image_size_threshold', '__return_false');
add_filter('jpeg_quality', function () {
    return 100;
});

// Auto-updates
add_filter('auto_update_core', '__return_true');
add_filter('auto_update_plugin', '__return_true');
add_filter('auto_update_theme', '__return_true');

// Remove email updates notifications
add_filter('auto_core_update_send_email', '__return_false');
add_filter('auto_plugin_update_send_email', '__return_false');
add_filter('auto_theme_update_send_email', '__return_false');
