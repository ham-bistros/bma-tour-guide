<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>

        <?php if (!is_user_logged_in()) : ?>
        <style>
.maplibregl-ctrl.maplibregl-ctrl-group .maplibregl-export-control {
    display: none;
}

        </style>
        <?php endif; ?>

  </head>
<body <?php body_class(); ?>>

<nav class="nav-menu">

<?php if (is_user_logged_in()) : ?>

<h2>Cartes</h2>
<ul>

<?php
  $args = array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'balade-carte.php'
  );
    ?>

                <!-- nav -->
    <?php foreach(get_pages($args) as $page) : ?>
        <li>
            <a href="<?= get_page_link($page->ID) ?>">
                <?= $page->post_title; ?>
            </a>

        </li>

    <?php endforeach ?>
</ul>

<h2>Dépliants</h2>
<ul>
    <?php foreach(get_categories() as $cat) : ?>
        <?php if ($cat->slug != "uncategorized") : ?>
            <li>
                <a href="<?= get_category_link($cat) ?>">
                    <?= $cat->name ?>
                </a>
            </li>
        <?php endif ?>
    <?php endforeach ?>

</ul>

<?php else : ?>
    <ul class="public-menu">
        <?php foreach(get_categories() as $categ) : ?>
            <?php if ($categ->slug != "uncategorized") : ?>

                <li>
                    <h3> <?= $categ->name ?> </h3>
                    <p>
                        <a class="zoomto <?= $categ->slug ?>" href="#<?= $categ->slug ?>">
                           Tour</a> •
                        <a href="<?= get_category_link($categ) ?>" target="_blank">Guide</a>
                    </p>
                </li>

            <?php endif ?>

        <?php endforeach ?>

    </ul>

<?php endif; ?>

</nav>

<?php foreach(get_categories() as $cat) : ?>
    <?php if ($cat->slug != "uncategorized") : ?>

        <div class="balade-intro <?= $cat->slug ?>">
            <h2><?= $cat->name ?></h2>
        </div>

    <?php endif ?>
<?php endforeach ?>

        <div id="map"></div>
        <button id="zoomto" class="btn-control">Zoom to bounds</button>

        <?php wp_footer(); ?>
</body>
