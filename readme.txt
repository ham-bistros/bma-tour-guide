=== Theme Name ===
Requires at least: 5.0
Tested up to: 8.3
Requires PHP: 5.6
License: GPLv3 or later
License URI: <https://www.gnu.org/licenses/gpl-3.0.html>

Theme for the BMA Tour Guide.

== Description ==
Theme for the BMA Tour Guide.

== Changelog ==

= 1.0 =
* Initial commit

== Resources ==
* Alors
