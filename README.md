# bma-tour-guide

Wordpress theme for the BMA Tour Guide project.

An interactive map made with Maplibre GL JS, MapTiler and OpenStreetMap data. But also a web2print tool to print maps and a leaflet. The printed material is printed with two spot colors. The tooling involves outputting 2 black pdfs and converting them to spot colors with [PDFutils](https://github.com/osp/osp.tools.PDFutils).

Graphic design by Kidnap Your Designer.
