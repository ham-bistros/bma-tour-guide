<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>

<?php
/**
    *   TEMPLATE POUR LES DÉPLIANTS
    *
    */

$category = get_category(get_query_var('cat'))->term_id;

$total_pages = (int)get_field('page_count', 'category_' . $category);

$pages_number = (is_user_logged_in()) ? $total_pages : 2;

$volets = $total_pages / 2;
$impair = $volets % 2;
$page_count =  $pages_number * 105 / 2;
$cover_text = get_field('cover_text', 'category_' . $category);
$non_detaille_number = get_field('non-detailed_pages', 'category_' . $category);
$insert_index = (1 + $total_pages / 2) - $non_detaille_number; // To find the position of non-detailed pages

if ($impair && $non_detaille_number == 1) {
    $insert_index -= 1;
}

$projets = get_posts(array("category" => $category, "post_type" => "post", "numberposts" => -1));

$non_detaille = get_posts(array(
"category" => $category,
"numberposts" => -1,
"post_type" => 'non-detaille'));

$eat_drink = get_posts(array(
        "category" => $category,
        "numberposts" => -1,
        "post_type" => 'eat-drink'));


$intro_text = get_field('intro_text', 'category_' . $category);
$cover_text = get_field('cover_text', 'category_' . $category);
$more_info_left = get_field('more_info_left', 'category_' . $category);
$more_info_right = get_field('more_info_right', 'category_' . $category);
$logo = get_field('logo', 'category_' . $category);
$map_spot_1 = get_field('map_spot_1', 'category_' . $category);
$map_spot_2 = get_field('map_spot_2', 'category_' . $category);
$institutions_support = get_field('institutions_support', 'category_' . $category);

?>

<style>
@media print {
  @page {

  <?php if (is_user_logged_in()) : ?>
    marks: crop cross;
  <?php endif; ?>

  <?= 'size: ' . $page_count . 'mm 148.5mm;'?>
  margin: 0;
  }
}
</style>

</head>

<body <?php body_class(); ?>>
  <article id="recto">

    <!-- Displays the cover infos only for the public version -->
  <?php if (!is_user_logged_in()) : ?>
      <section class="couv-1e spot1">
      <div class="cercle">
          <h1><?= single_cat_title() ?></h1>
        </div>
        <div class="couv-images">
            <?php foreach($projets as $page) : ?>

            <?php
    $img_url = wp_get_attachment_url(get_post_thumbnail_id($page->ID), 'thumbnail');
                ?>

                <div class="image" >
                  <img src="<?= $img_url ?>" />
                  <aside class="img-color"></aside>
              </div>
            <?php endforeach; ?>

        </div>

	<svg class="svg-couv tour-guide" viewBox="0 0 297.6 419.5">
          <?php
          $fichier = '';

      switch ($cover_text["value"]) {
          case 'bma':
              $fichier = 'bma';
              break;

          case 'fga-vb':
              $fichier = 'vb';
              break;

          case 'fgb-wbf':
              $fichier = 'fga-fwb';
              break;

          case 'c-bma':
              $fichier = 'cb';
              break;

          default:
              $fichier = 'tour-guide';
              break;
      }

      $filepath = get_site_url() . '/wp-content/themes/bma-tour-guide/assets/covers/' . $fichier . '.svg#tour-guide';
      ?>

        <use xlink:href="<?= $filepath ?>" fill="white"/>
	</svg>

      </section>

      <?php if ($more_info_left || $more_info_right) : ?>
        <section class="page more_info">

        <div class="more_info_left">
          <?php if ($more_info_left) : ?>
            <?= $more_info_left ?>
          <?php endif ?>
        </div>


        <div class="more_info_right">
          <?php if ($more_info_right) : ?>
            <?= $more_info_right ?>
          <?php endif ?>
        </div>
        </section>

        <?php endif; ?>
      <?php endif; ?>

      <!-- the loop -->
  <?php foreach ($projets as $index => $page) : ?>
    <?php if ($index == $insert_index && $non_detaille_number) : ?>
      <section id="non-detaille" class="page non-detaille">

        <svg class="svg-couv nb-bm" viewBox="0 0 297.6 419.5">
          <use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/covers/nd-bm.svg#tour-guide" fill="#004c5d"/>
	</svg>

  <?php foreach ($non_detaille as $i => $nd) : ?>
        <div class="nd-single">
            <aside class="pointeur <?php if (get_field('viewpoint', $nd->ID)) {
                echo 'viewpoint';
            } ?>">

            <?php if (get_field('index', $nd)) : ?>
                <?php if (get_field('viewpoint', $nd->ID)) : ?>
                <!-- display eye svg -->
                <svg class="svg-viewpoint" viewBox="0 0 12.4 8.8000002">
                        <use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/pointeurs/Pointeur-oeil_01.svg#pointeur-oeil" fill="white"/>
                </svg>

                <?php else : ?>
                <!-- display other svg -->
                <svg class="svg-non-detaille" viewBox="0 0 9 13">
                        <use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/pointeurs/Pointeur-ND_01.svg#pointeur-nd" fill="white"/>
                </svg>

              <?php endif ?>

            <p class="index"><?= get_field('index', $nd->ID); ?></p>
	<?php else : ?>
            <p style="font-size: var(--text-smol);"><?= $i + 1 ?> ---</p>
	<?php endif ?>
          </aside>

          <div class="infos-complementaires">
	    <span class="title"><?= trim(get_the_title($nd->ID, 'post')) ?></span>

            <?php if (get_field('conception', 'post_'.$nd->ID)) : ?>
              <span class="conception nope"><?= trim(get_field('conception', 'post_'.$nd->ID)); ?></span>
            <?php endif ?>

            <?php if (get_field('prime_contractor', 'post_'.$nd->ID)) : ?>
              <span class="prime_contractor"><?= trim(get_field('prime_contractor', 'post_'.$nd->ID)) ;?></span>
            <?php endif ?>

            <?php if (get_field('program', 'post_'.$nd->ID)) : ?>
              <span><?= trim(get_field('program', 'post_'.$nd->ID)) ; ?></span>
            <?php endif ?>

            <?php if (get_field('date', 'post_'.$nd->ID)) : ?>
              <span><?= trim(get_field('date', 'post_'.$nd->ID)) ; ?></span>
            <?php endif ?>

            <span class="nope"> <?= trim(get_field('adress', 'post_'.$nd->ID)) ; ?></span>
          </div>
        </div>
  <?php endforeach; ?>

  <?php if ($non_detaille_number == 2) : ?>
          <div class="fondperdu spot2"></div>
        </section>
        <section class="non-detaille">

          <svg class="svg-couv nb-bm" viewBox="0 0 297.6 419.5">
            <use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/covers/nd-bm.svg#tour-guide" fill="#004c5d"/>
          </svg>

  <?php endif; ?>

  <?php foreach($eat_drink as $index => $bm) : ?>
        <div class="ed-single">
          <aside class="pointeur eat-drink">
	<!-- display eat-drink svg -->
	<svg class="svg-eat-drink" viewBox="0 0 6 6">
		<use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/pointeurs/Pointeur-BM_01.svg#pointeur-bm" fill="white"/>
	</svg>
	<p class="index"><?= $index + 1 ?></p>
  </aside>

          <div class="infos-complementaires">
	<span class="title">
		<svg class="ed-icon" viewBox="0 0 16 16">
			<use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/osm-pictos/<?= get_field('type', 'post_'.$bm->ID) ?>.svg#<?= get_field('type', 'post_'.$bm->ID) ?>" fill="white"/>
		</svg>
		<?= get_the_title($bm->ID, 'post') ?>
	</span>
	    <span> <?= trim(get_field('adress', 'post_'.$bm->ID)); ?></span>

	<?php if (get_field('opening_hours', 'post_'.$bm->ID)) : ?>
            <span class="opening_hours"><?= trim(get_field('opening_hours', 'post_'.$bm->ID)); ?></span>
	<?php endif ?>

	<?php if (get_field('phone', 'post_'.$bm->ID)) : ?>
            <span class="phone"><?= trim(get_field('phone', 'post_'.$bm->ID)); ?></span>
	<?php endif ?>

	<?php if (get_field('website', 'post_'.$bm->ID)) : ?>
            <span class="website nope"><?php echo substr(get_field('website', 'post_'.$bm->ID), 8, null); ?></span>
	<?php endif ?>

          </div>

        </div>

	<?php endforeach ?>
        <div class="fondperdu spot2"></div>
      </section>

      <?php if ($non_detaille_number > 2) : ?>
          <?php foreach (range(1, $non_detaille_number - 1) as $i) : ?>
            <section class="non-detaille">

        <svg class="svg-couv nb-bm" viewBox="0 0 297.6 419.5">
          <use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/covers/nd-bm.svg#tour-guide"/>
	</svg>

            </section>
          <?php endforeach ?>
      <?php endif; ?>

    <?php endif; ?>

      <!-- Loop for posts -->
    <section id="<?= $page->post_name ?>" class="page projet">
        <div class="left">
          <header>
            <aside class="pointeur"><?= get_field('index', 'post_'.$page->ID); ?></aside>
            <h2>
              <?= $page->post_title ?>
            </h2>
          </header>

            <?php

            $img_url = wp_get_attachment_url(get_post_thumbnail_id($page->ID), 'thumbnail');
      $img_metadata = wp_get_attachment_metadata(get_post_thumbnail_id($page->ID), 'thumbnail');

      if ($img_url) {
          $new_width = 159.21; // width de base dans la page imprimée
          $ratio = $img_metadata["width"] / $new_width;
          $new_height = $img_metadata["height"] / $ratio;

          $area = $new_width * $new_height;
          $wanted_area = 68250; // On peut modifier ce nombre pour augmenter ou diminuer le scaling
          $scaling = $wanted_area / $area;
      }

      ?>

          <!-- sqrt() parce que la surface évolue au carré de la hauteur/largeur -->
          <div class="image spot2" style="--scaling-factor: <?= sqrt($scaling) ?>; --img-width: <?= $new_width ?>px; --img-height: <?= $new_height  ?>px;">
            <?php if ($img_url) : ?>
              <img src="<?= $img_url ?>" />
            <?php endif; ?>

            <aside class="img-color"></aside>
          </div>

          <div class="infos-complementaires spot1">

            <?php if (get_field('conception', 'post_'.$page->ID)) : ?>
              <span class="conception nope"><?= trim(get_field('conception', 'post_'.$page->ID)) ; ?></span>
            <?php endif ?>

            <?php if (get_field('prime_contractor', 'post_'.$page->ID)) : ?>
              <span class="prime_contractor"> <?= trim(get_field('prime_contractor', 'post_'.$page->ID)) ; ?></span>
            <?php endif ?>

            <?php if (get_field('program', 'post_'.$page->ID)) : ?>
              <span><?= trim(get_field('program', 'post_'.$page->ID)) ; ?></span>
            <?php endif ?>

            <?php if (get_field('date', 'post_'.$page->ID)) : ?>
              <span class="nope"><?= trim(get_field('date', 'post_'.$page->ID)) ; ?></span>
            <?php endif ?>

              <?php if (get_field('link', 'post_'.$page->ID)) : ?>
              <?php
                  $link = trim(get_field('link', 'post_'.$page->ID));
                  $shortlink = str_replace(parse_url($link, PHP_URL_SCHEME) . '://', '', $link);
                  ?>
              <span class="nope"><br>link: <a href="<?= $link?>"><?= $shortlink ?></a></span>
              <?php endif ?>

            <p class="link-credits">
              <span class="nope">© <?= trim(get_field('photo_credit', 'post_'.$page->ID)) ; ?></span>
            </p>

            <p><?= trim(get_field('adress', 'post_'.$page->ID)) ; ?></p>
          </div>

        </div>

      <div class="right spot1">
          <!-- Filter to add auto paragraphs tags -->
          <?php
      echo apply_filters("the_content", $page->post_content);
      ?>

          <pre>
          </pre>

      </div>
    </section>
  <?php endforeach; ?>

      <section class="page couv">

        <div class="cercle spot2">
          <h1><?= single_cat_title() ?></h1>
        </div>

        <div class="couv-images spot2">
            <?php foreach($projets as $page) : ?>
                <div class="image" >
                  <?php $img_url = wp_get_attachment_url(get_post_thumbnail_id($page->ID), 'thumbnail'); ?>
                  <img src="<?= $img_url ?>" />

                <aside class="img-color"></aside>
              </div>
            <?php endforeach;?>

        </div>
        <div class="fondperdu spot2"></div>

	<svg class="svg-couv tour-guide spot2" viewBox="0 0 297.6 419.5">
          <?php
      $fichier = '';

switch ($cover_text["value"]) {
    case 'bma':
        $fichier = 'bma';
        break;

    case 'fga-vb':
        $fichier = 'vb';
        break;

    case 'fgb-wbf':
        $fichier = 'fga-fwb';
        break;

    case 'c-bma':
        $fichier = 'cb';
        break;

    default:
        $fichier = 'tour-guide';
        break;
}

$filepath = get_site_url() . '/wp-content/themes/bma-tour-guide/assets/covers/' . $fichier . '.svg#tour-guide';
?>

        <use xlink:href="<?= $filepath ?>" fill="white"/>
	</svg>

      </section>

    <?php if (!is_user_logged_in()) : ?>
      <section class="couv-4e">

        <div class="intro-text">
          <?= $intro_text ?>
        </div>

      <div class="credits">

        <p class="qrcode">Find a web version of this map and other tours through this link: <br/><a href="https://rb.gy/mpyb56">rb.gy/mpyb56</a></p>
          <!-- QR CODE À REMPLACER -->
        <!-- <img src="https://cdn.kibrispdr.org/data/910/white-qr-code-png-35.png" alt="qr code" /> -->
          <svg viewBox="0 0 1155 1155">
            <use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/qr-code.svg#qrcode" />
          </svg>

          <?php if ($logo) : ?>
            <div class="logo" style="background-image: url('<?= $logo ?>');"></div>
          <?php endif ?>


          <?php if ($institutions_support) : ?>
             <?= $institutions_support ?>
          <?php endif ?>

        <p class="real-credits">Design & Dev: Kidnap your designer with Thomas Bris • Print: Drifosett • © MapTiler © OpenStreetMap Contributors © WaterGIS
        </p>

      </div>

        <svg class="svg-couv back-cover" viewBox="0 0 297.6 419.5">
          <use xlink:href="<?= get_site_url() ?>/wp-content/themes/bma-tour-guide/assets/covers/dos.svg#tour-guide" />
	</svg>

      </section>
    <?php endif; ?>


  </article>

    <?php wp_footer(); ?>
</body>
